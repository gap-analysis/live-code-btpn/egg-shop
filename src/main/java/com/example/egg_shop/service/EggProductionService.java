package com.example.egg_shop.service;

import com.example.egg_shop.dto.EggProductionDTO;
import com.example.egg_shop.entity.EggProduction;
import com.example.egg_shop.repository.EggProductionRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class EggProductionService {

    @Autowired
    EggProductionRepository eggProductionRepository;

    public ResponseEntity<?> getAllProduction(){
        List listEggProductions = eggProductionRepository.findAll();

        return ResponseEntity.ok(listEggProductions);
    }

    public ResponseEntity<?> addProduction(EggProductionDTO request){
        EggProduction eggProduction = new EggProduction();
        eggProduction.setQuantity(request.getQuantity());
        eggProductionRepository.save(eggProduction);

        return ResponseEntity.ok(eggProduction);
    }

    public ResponseEntity<?> eggRecapitulation (){
        int total = 0;

        for (EggProduction egg: eggProductionRepository.findAll()) {
            total += egg.getQuantity();
        }

        return ResponseEntity.ok("Total rekapitulasi produksi telur : " + total);
    }
}
