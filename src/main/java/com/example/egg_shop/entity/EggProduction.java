package com.example.egg_shop.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Entity
@Table(name = "egg_production")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EggProduction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "date")
    private Date date = new Date();

    @Column(name = "quantity")
    private int quantity;
}
