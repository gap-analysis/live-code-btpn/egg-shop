package com.example.egg_shop.repository;

import com.example.egg_shop.entity.EggProduction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EggProductionRepository extends JpaRepository<EggProduction, Long> {
}
