package com.example.egg_shop.controller;

import com.example.egg_shop.dto.EggProductionDTO;
import com.example.egg_shop.service.EggProductionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/egg-shop")
public class EggProductionController {

    @Autowired
    EggProductionService eggProductionService;

    @GetMapping("/get")
    @ResponseBody
    public ResponseEntity<?> getAllProduction (){
        return eggProductionService.getAllProduction();
    }

    @PostMapping("/add")
    @ResponseBody
    public ResponseEntity<?> addProduction (@RequestBody EggProductionDTO request){
        return eggProductionService.addProduction(request);
    }

    @GetMapping("/recapitulation")
    @ResponseBody
    public ResponseEntity<?> recapitulation (){
        return eggProductionService.eggRecapitulation();
    }
}
